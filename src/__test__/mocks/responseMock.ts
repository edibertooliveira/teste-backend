export const password =
  '$2a$08$JST6i/.s2uKZbgefdgz.U.fqneH6DFZZqjGrC3Gm03a7lcoJ.kamK';

export const users = [
  {
    name: 'user 1',
    email: 'user1@user.com',
    password,
    role: 'customer',
  },
  {
    name: 'user 2',
    email: 'user2@user.com',
    password,
    role: 'customer',
  },
  {
    name: 'user 3',
    email: 'user3@user.com',
    password,
    role: 'customer',
  },
  {
    name: 'user 4',
    email: 'user4@user.com',
    password,
    role: 'customer',
  },
  {
    name: 'user 5',
    email: 'user5@user.com',
    password,
    role: 'customer',
  },
  {
    name: 'user 6',
    email: 'user6@user.com',
    password,
    role: 'customer',
  },
];

export const movies = [
  {
    title: '2001: A Space Odyssey',
    genre: 'science fiction',
    directors: 'Stanley Kubrick',
    authors: 'Stanley Kubrick, Arthur C Clarke',
    year: 2014,
  },
  {
    title: 'The Godfather',
    genre: 'thriller',
    directors: 'Francis Ford Coppola',
    authors: 'Mario Puzo, Francis Ford Coppola',
    year: 1972,
  },
  {
    title: 'Raiders of the Lost Ark',
    genre: 'action adventure',
    directors: 'Steven Spielberg',
    authors: 'Lawrence Kasdan',
    year: 1981,
  },
  {
    title: ' Singin’ in the Rain',
    genre: 'comedy',
    directors: 'Stanley Donen, Gene Kelly',
    authors: 'Adolph Green, Betty Comden',
    year: 1952,
  },
  {
    title: 'The Dark Knight',
    genre: 'action adventure',
    directors: 'Christopher Nolanl',
    authors: 'Jonathan Nolan, Christopher Nolan',
    year: 2008,
  },
  {
    title: 'The Searchers',
    genre: 'action adventure',
    directors: 'John Ford',
    authors: 'Frank S Nugent',
    year: 1956,
  },
  {
    title: 'Star Wars',
    genre: 'science fiction',
    directors: 'George Lucas',
    authors: 'George Lucas',
    year: 1977,
  },
  {
    title: 'Alien',
    genre: 'science fiction',
    directors: 'Ridley Scott',
    authors: 'Dan O`Bannon',
    year: 1975,
  },
  {
    title: 'Lawrence of Arabia',
    genre: 'drama',
    directors: 'David Lean',
    authors: 'Robert Bolt',
    year: 1964,
  },
  {
    title: 'Out of Sight',
    genre: 'thriller',
    directors: 'Steven Soderbergh',
    authors: 'Scott Frank',
    year: 1998,
  },
];
