import CreateMovieService from './CreateMovieService';
import DeleteMovieService from './DeleteMovieService';
import ListMoviesService from './ListMoviesService';
import ShowMovieService from './ShowMovieService';
import UpdateMovieService from './UpdateMovieService';
import VoteMovieService from './VoteMovieService';
import SearchMoviesService from './SearchMoviesService';
import UpdateMovieImageService from './UpdateMovieImageService';

export {
  CreateMovieService,
  DeleteMovieService,
  ListMoviesService,
  ShowMovieService,
  UpdateMovieService,
  VoteMovieService,
  SearchMoviesService,
  UpdateMovieImageService,
};
